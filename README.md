# Install Netdata

https://app.netdata.cloud -> Settings -> Nodes

Get the ops server #netdata-alarms webhook

vi /etc/netdata/health_alarm_notify.conf

```
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discordapp.com/api/webhooks/XXXXXXXXXXXXX/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
```

vi /usr/libexec/netdata/plugins.d/alarm-notify.sh
Add this to the alarm text: <@&503340901662457871>

To test webhook works correctly:
/usr/libexec/netdata/plugins.d/alarm-notify.sh test
